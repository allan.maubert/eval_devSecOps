FROM ubuntu:bionic

RUN apt-get update && apt-get install -y --no-install-recommends \
    curl=7.58.0-2ubuntu3.12 \
    git=1:2.17.1-1ubuntu0.7 \
    python3=3.6.7-1~18.04 \
    nginx=1.14.0-0ubuntu1.7 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN addgroup esgi

RUN useradd -rm -d /home/user -s /bin/bash -g esgi -u 1001 jordan
RUN useradd -rm -d /home/user -s /bin/bash -g esgi -u 1002 jonathan

COPY ./index.html /usr/share/nginx/html/index.html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]