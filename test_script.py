import subprocess

outErr = open('logs/std_logs.txt', 'a')


def write_file(message, returncode):
    outErr.write(str(message) + str(returncode) + '\n')


process = subprocess.Popen('docker exec test git --version', cwd='./', shell=True, stderr=subprocess.STDOUT,
                           stdout=outErr)
process.wait()
write_file("Git Version Test return code : ", process.returncode)

if process.returncode != 0:
    exit(1)

process = subprocess.Popen('docker exec test python3 --version', cwd='./', shell=True, stderr=subprocess.STDOUT,
                           stdout=outErr)
process.wait()
write_file("Python3 Version Test return code : ", process.returncode)

if process.returncode != 0:
    exit(1)

process = subprocess.Popen('docker exec test curl 127.0.0.1/index.html', cwd='./', shell=True, stderr=subprocess.STDOUT,
                           stdout=outErr)
process.wait()
write_file("Nginx index Test return code : ", process.returncode)

if process.returncode != 0:
    exit(1)

process = subprocess.Popen('docker exec test grep jordan /etc/passwd', cwd='./', shell=True, stderr=subprocess.STDOUT,
                           stdout=outErr)
process.wait()
write_file("jordan user Test return code : ", process.returncode)

if process.returncode != 0:
    exit(1)

process = subprocess.Popen('docker exec test grep jonathan /etc/passwd', cwd='./', shell=True, stderr=subprocess.STDOUT,
                           stdout=outErr)
process.wait()
write_file("jonathan user Test return code : ", process.returncode)

if process.returncode != 0:
    exit(1)

process = subprocess.Popen('docker exec test grep esgi /etc/group', cwd='./', shell=True, stderr=subprocess.STDOUT,
                           stdout=outErr)
process.wait()
write_file("esgi group Test return code : ", process.returncode)

if process.returncode != 0:
    exit(1)

